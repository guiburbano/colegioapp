from django.shortcuts import render
from django.http import HttpResponse
from colegio.models import Grado, Estudiante
from django.views.generic import ListView, DetailView 
from django.shortcuts import render 
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import UpdateView, CreateView, DeleteView 

# Create your views here.
@login_required
def first_view(request):
    #return HttpResponse("<H1>HOLA MUNDO DESDE LA VISTA first_view</H1>")
    return render(request, 'base.html')

@login_required
def grado(request):
    grado_list = Grado.objects.all()
    context = {'object_list': grado_list}
    return render(request, 'colegio/grado.html', context)

@login_required
def grado_detail(request, grado_id):
    grado = Grado.objects.get(id=grado_id)
    context = {'object': grado}
    return render(request, 'colegio/grado_detail.html', context)

@login_required
def grado_list(request, grado_id):
    grado = Grado.objects.get(id=grado_id)
    context = {'object': grado}
    return render(request, 'colegio/grado_list.html', context)
    
@method_decorator(login_required, name='dispatch')
class EstudianteListView(ListView):
    model = Estudiante

@method_decorator(login_required, name='dispatch')
class EstudianteDetailView(DetailView):
    model = Estudiante

def base(request):
     return render(request, 'base.html')

# --------------------------- GRADOS ---------------------------------

class GradoList(ListView):
    model = Grado

class GradoCreate(CreateView):
    model = Grado
    fields = '__all__'
    success_url = reverse_lazy('grado-list')

class GradoUpdate(UpdateView):
    model = Grado
    fields = '__all__'
    success_url = reverse_lazy('grado-list')

class GradoDetail(DetailView):
    model = Grado

class GradoDelete(DeleteView):
    model = Grado
    success_url = reverse_lazy('grado-list')


# --------------------------- ESTUDIANTES ---------------------------------

class EstudianteList(ListView):
    model = Estudiante

class EstudianteCreate(CreateView):
    model = Estudiante
    fields = '__all__'
    success_url = reverse_lazy('estudiante-list')

class EstudianteUpdate(UpdateView):
    model = Estudiante
    fields = '__all__'
    success_url = reverse_lazy('estudiante-list')

class EstudianteDetail(DetailView):
    model = Estudiante

class EstudianteDelete(DeleteView):
    model = Estudiante
    success_url = reverse_lazy('estudiante-list')

def EstudianteListFilter(request, grado_id):
    grado = Grado.objects.filter(id=grado_id) 
    estudiantes = Estudiante.objects.filter(grado=grado_id)
    context = {'estudiantes_data':estudiantes, 'grado_data':grado}
    return render(request, 'colegio/estudiante_filter_list.html', context)

#class EstudianteList(viewsets.ModelViewSet):
	#queryset=Estudiante.objects.all()
	#serializer_class=EstudianteSerializer

#class GradoList(viewsets.ModelViewSet):
    #queryset = Grado.objects.all()
   # serializer_class = GradoSerializer