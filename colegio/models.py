from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver  
from django.urls import reverse

# Create your models here.
class Grado(models.Model):
    grado = models.CharField(max_length= 10, default='')
    anio = models.CharField(max_length= 10, default='')
    
    def __str__(self):
     return self.grado +" "+ self.anio

class Horario(models.Model):
    fotoHorario = models.ImageField(upload_to='fotos/')

    def __str__(self):
        return str(self.fotoHorario)

class  Estudiante(models.Model):
    carnet = models.CharField(max_length=50, default='')
    nombre = models.CharField(max_length=50, default='')
    apellido = models.CharField(max_length=50, default='')
    seguro = models.CharField(max_length=50, default='')
    foto = models.ImageField( upload_to='fotos/')
    fecha_naci = models.DateField(auto_now_add= False)
    correo = models.CharField(max_length=50, default='')
    grado = models.ForeignKey(Grado, on_delete=models.PROTECT)
    fotoHorario = models.ForeignKey(Horario, on_delete=models.PROTECT)
    
    def __str__(self):
         return self.carnet +" "+ self.nombre +" "+ self.apellido +" "+ self.seguro +" "+ self.correo
    def get_str_url(self):
     return reverse('estudiante-list')

@receiver(post_delete, sender=Estudiante)
def eliminar_Estudiante(sender, instance, **kwargs):
        instance.foto.delete(False)

