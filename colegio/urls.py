from django.urls import path, include
from colegio import views
from django.contrib import admin




urlpatterns = [ 

    # Main view
    path('', views.base, name='base'),

    # --------------------------- GRADOS ---------------------------------

    # List
    path('grados/', views.GradoList.as_view(), name='grado-list'), 
    
    # Create
    path('grado/create', views.GradoCreate.as_view(), name='grado-create'),
    
    # Update
    path('grado/<int:pk>/update', views.GradoUpdate.as_view(), name='grado-update'),
    
    # Detail
    path('grado/<int:pk>/detalles', views.GradoDetail.as_view(), name='grado-detail'),
    
    # Delete
    path('grado/<int:pk>/delete', views.GradoDelete.as_view(), name='grado-delete'),

    # --------------------------- ESTUDIANTES ---------------------------------

    # List
    path('estudiantes/', views.EstudianteList.as_view(), name='estudiante-list'),

    # Filter estudiantes
    path('estudiantes/<int:grado_id>/lista', views.EstudianteListFilter, name='estudiante-filter-list'),
    
    # Create
    path('estudiante/create/', views.EstudianteCreate.as_view(), name='estudiante-create'),
    
    # Detail
    path('estudiante/<int:pk>/detalles', views.EstudianteDetail.as_view(), name='estudiante-detail'), 

    # Update
    path('estudiante/<int:pk>/actualizar/', views.EstudianteUpdate.as_view(),name='estudiante-update'),
    
    # Delete
    path('estudiante/<int:pk>/eliminar/', views.EstudianteDelete.as_view(),name='estudiante-delete'),

]