from django.contrib import admin
from colegio.models import Grado, Horario, Estudiante

class EstudianteAdmin(admin.ModelAdmin):
    list_display = ('carnet', 'nombre', 'apellido','seguro','foto','fecha_naci','correo','grado')
    
class GradoAdmin(admin.ModelAdmin):
    list_display = ('grado', 'anio')

# Register your models here.
admin.site.register(Grado, GradoAdmin)
admin.site.register(Horario)
admin.site.register(Estudiante, EstudianteAdmin)

